from BrokerTransactions import BrokerTransactions
from FXRates import FXRates

def main():
    transactions = BrokerTransactions()
    fx_rates = FXRates()

if __name__ == '__main__':
    main()